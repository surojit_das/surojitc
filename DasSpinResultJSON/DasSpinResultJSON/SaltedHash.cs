﻿using System;
using System.Text;
using System.Security.Cryptography;
namespace DasSpinResultJSON
{
    static class SaltedHash
    {

        public static byte[] copyBytes32(byte []salt)
        {
            var saltBytes = new byte[32];
            for(int i=0;i<32;i++)
            {
                if (i < salt.Length)
                    saltBytes[i] = salt[i];
            }
            return saltBytes;
        }
        public static string GetHashSHA1(byte [] salt, string value)
        {
            var saltBytes = copyBytes32(salt);
            string hashResult = string.Empty;
            Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(value, saltBytes, 999);

            hashResult = Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256));
            return hashResult;
        }
        public static string GetHashSHA256(byte[] salt, string value)
        {
            string hashResult = string.Empty;

            var saltBytes = copyBytes32(salt);
            SHA256Managed hash = new SHA256Managed();

            byte[] valueBytes = UTF8Encoding.UTF8.GetBytes(value);
            byte[] hashByte = new byte[saltBytes.Length + valueBytes.Length];
            System.Buffer.BlockCopy(saltBytes, 0, hashByte, 0, saltBytes.Length);
            System.Buffer.BlockCopy(valueBytes, 0, hashByte, saltBytes.Length, valueBytes.Length);

            for (int i = 0; i < 999; i++)
            {
                hashByte = hash.ComputeHash(hashByte);
            }
            byte[] afterHashedByte = new byte[valueBytes.Length + hashByte.Length];
            System.Buffer.BlockCopy(valueBytes, 0, afterHashedByte, 0, valueBytes.Length);
            System.Buffer.BlockCopy(hashByte, 0, afterHashedByte, valueBytes.Length, hashByte.Length);
            hashResult = Convert.ToBase64String(afterHashedByte);

            return hashResult.Split('+')[1];
        }

        public static bool VerifySHA1(byte[] saltbytes , string hash, string value)
        {
            return hash == GetHashSHA1(saltbytes, value);
        }
        public static bool VerifySHA256(byte[] saltbytes, string hash, string value)
        {
            return hash == GetHashSHA256(saltbytes, value);
        }
        public static int VerifyHash(string hash1, string hash2)
        {
            //return hash1 == hash2;
            if (hash1.Length != hash2.Length)
                return -1;
            char[] arr1 = hash1.ToCharArray();
            char[] arr2 = hash2.ToCharArray();
            for(int i=0;i<hash1.Length;i++)
            {
                if (arr1[i] != arr2[i])
                    return i;
            }
            return 0;
        }
    }
}
