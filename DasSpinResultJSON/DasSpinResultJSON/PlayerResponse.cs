﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DasSpinResultJSON
{
    public class PlayerResponse
    {
        public int player_id { get; set; }
        public string Player_name { get; set; }
        public Nullable<double> credits { get; set; }
        public Nullable<long> lifetime_spins { get; set; }
        public Nullable<float> lifetime_avg_return { get; set; }
    }
    public class PlayerRequest
    {
        public int player_id { get; set; }
        public string hash { get; set; }
        public Nullable<double> coins_won { get; set; }
        public Nullable<double> coins_bet { get; set; }
    }
    public class PlayerViewData
    {
        public int player_id { get; set; }
        public string Player_name { get; set; }
        public Nullable<double> credits { get; set; }
        public Nullable<double> credits_won { get; set; }
        public Nullable<long> lifetime_spins { get; set; }
        public string hash { get; set; }
    }

}
