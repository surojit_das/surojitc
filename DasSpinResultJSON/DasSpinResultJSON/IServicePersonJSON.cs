﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DasSpinResultJSON
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IServicePersonJSON" in both code and config file together.
    [ServiceContract]
    public interface IServicePersonJSON
    {
        [OperationContract]
        [WebInvoke(Method ="GET", UriTemplate = "findplayer/{playerID}", ResponseFormat = WebMessageFormat.Json)]
        PlayerViewData FindPlayer(string playerID);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "help", ResponseFormat = WebMessageFormat.Json)]
        string Help();
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "createplayer", ResponseFormat = WebMessageFormat.Json)]
        bool CreatePlayer(PlayerEntity player);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "updateplayer/?hash={strHash}&coinswon={strCoinsWon}&coinsbet={strCoinsBet}&playerid={strPlayerId}", ResponseFormat = WebMessageFormat.Json)]//[WebGet(UriTemplate = "?id={id}&value={value}")]
        PlayerResponse UpdatePlayer(string strHash,string strCoinsWon,string strCoinsBet, string strPlayerId);
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "updateplayer", ResponseFormat = WebMessageFormat.Json)]
        PlayerResponse UpdatePlayerByRequest(PlayerRequest request);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "deleteplayer", ResponseFormat = WebMessageFormat.Json)]
        bool DeletePlayer(PlayerEntity player);
    }
}
