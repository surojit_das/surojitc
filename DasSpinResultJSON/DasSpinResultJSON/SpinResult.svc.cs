﻿using System;
using System.Linq;

namespace DasSpinResultJSON
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServicePersonJSON" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServicePersonJSON.svc or ServicePersonJSON.svc.cs at the Solution Explorer and start debugging.
    [System.ServiceModel.ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class ServicePersonJSON : IServicePersonJSON
    {
        public bool CreatePlayer(PlayerEntity player)
        {
            using (DasSpinResultEntities spinResultE = new DasSpinResultEntities())
            {
                try
                {
                    spinResultE.PlayerEntities.Add(player);
                    spinResultE.SaveChanges();
                    return true;

                }
                catch (Exception ex)
                {
                    return false;
                }
                //throw new NotImplementedException();
            }
        }

        public bool DeletePlayer(PlayerEntity player)
        {
            throw new NotImplementedException();
        }
        public string Help()
        {
            return @"To FIND a player-> [site]|spinresult.svc|findplayer|{playerId}" + " |||| " +
                        @"To UPDATE a player-> [site]|updateplayer|?hash={strHash}&coinswon={strCoinsWon}&coinsbet={strCoinsBet}&playerid={strPlayerId}";

        }

        public PlayerViewData FindPlayer(string playerID)
        {
            using (DasSpinResultEntities spinResultE = new DasSpinResultEntities())
            {
                int pId = 0;
                bool isParsed = int.TryParse(playerID, out pId);
                PlayerViewData pv = new PlayerViewData();


                if (isParsed)
                {
                    try
                    {
                        PlayerEntity pe = spinResultE.PlayerEntities.Where(p => p.player_id == pId).First();

                        pv.player_id = pe.player_id;
                        pv.Player_name = pe.Player_name;
                        pv.credits = pe.credits;
                        pv.credits_won = pe.credits_won;
                        pv.lifetime_spins = pe.lifetime_spins;
                        pv.hash = SaltedHash.GetHashSHA256(pe.salt_value, pe.player_id.ToString());
                    }
                    catch (Exception ex)
                    {
                        pv.Player_name = "Player Not found";
                        pv.player_id = -1;
                        pv.credits = -1;
                        pv.credits_won = -1;
                        pv.lifetime_spins = -1;
                        pv.hash = ex.Message;

                        return pv;
                    }
                }

                return pv;

            }
            //throw new NotImplementedException();
        }

        public PlayerResponse UpdatePlayerByRequest(PlayerRequest request)
        {

            PlayerViewData playerFound = FindPlayer(request.player_id.ToString());
            PlayerResponse result = new PlayerResponse();
            if (playerFound.player_id <=0)
            {
                result.Player_name = "Player Not found";
                result.player_id = -1;
                result.credits = -1;
                result.lifetime_avg_return = -1;
                result.lifetime_spins = -1;
                return result;
            }
            if (!request.hash.Equals( playerFound.hash))
            {
                result.Player_name = "HASH MISMATCH: ";// + playerFound.hash;
                result.player_id = -1;
                result.credits = -1;
                result.lifetime_avg_return = -1;
                result.lifetime_spins = -1;
                return result;
            }

            using (DasSpinResultEntities spinResultE = new DasSpinResultEntities())
            {
                try
                {
                    PlayerEntity player = spinResultE.PlayerEntities.Single(p => p.player_id == playerFound.player_id);
                    player.credits -= Convert.ToDouble(request.coins_bet);
                    player.lifetime_spins++;
                    player.credits_won += Convert.ToDouble(request.coins_won);

                    result.player_id = player.player_id;
                    result.Player_name = player.Player_name;
                    result.credits = player.credits;
                    result.lifetime_spins = player.lifetime_spins;
                    result.lifetime_avg_return = (float)player.credits_won / player.lifetime_spins;
                    spinResultE.SaveChanges();
                }
                catch(Exception ex)
                {
                    //to do log the exception
                    return null;
                }
            }
            return result;
        }

        public PlayerResponse UpdatePlayer(string strHash, string strCoinsWon, string strCoinsBet, string strPlayerId)
        {
            PlayerRequest pr = new PlayerRequest()
            { player_id =Convert.ToInt32(strPlayerId), coins_won = Convert.ToDouble(strCoinsWon), coins_bet=Convert.ToDouble(strCoinsBet), hash=strHash };
            return UpdatePlayerByRequest(pr);
        }
    }
}
